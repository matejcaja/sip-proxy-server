#Zadanie

Naprogramujte SIP proxy server, ktorý bude schopný oblúžiť základné SIP správy a umožní používateľom zaregistrovať sa cez SIP telefón ako aj uskutočniť hovor medzi dvoma účastníkmi. SIP správy budú obsluhované Vašim proxu serverom, t.j. žiadne správy nepôjdu  priamo medzi účastníkmi.

Pre kompletnú dokumentáciu viď. súbor Dokumentácia.pdf