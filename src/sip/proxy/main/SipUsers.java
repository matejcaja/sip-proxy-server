package sip.proxy.main;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTable;



@SuppressWarnings("serial")
public class SipUsers extends JDialog implements ActionListener{

	private final JPanel contentPanel = new JPanel();
	JButton okButton = new JButton("Close");
	private final JScrollPane scrollPane = new JScrollPane();
	private JTable table = new JTable();
	Hashtable<String, String> users= new Hashtable<String, String>();
	
	public void updateTable(String username, String address){
		DefaultTableModel tableModel = (DefaultTableModel) this.table.getModel();
		tableModel.addRow(new Object[] {username,address});
	}
	
	public void handleUserRegister(String[] contact, int expireValue){
		if(users.containsKey(contact[2]) == false && expireValue != 0){
    		users.put(contact[2], contact[2] +"@"+ contact[3]+":"+contact[4]);
    		updateTable(contact[2], contact[2] +"@"+ contact[3]+":"+contact[4]);
    		//if(DEBUG)jTextArea.append("\nDEBUG: saving user " + contact[2] +"@"+ contact[3]+":"+contact[4];
    		
    	}else if(users.containsKey(contact[2]) == true && expireValue == 0){
    		users.remove(contact[2]);
    		removeFromTable(contact[2]);
    		//if(DEBUG)jTereturn true;
    	}
    		
		
		

	}
	
	public String getUser(String key){
		return users.get(key);
		
	}
	
	public void removeFromTable(String username){
		DefaultTableModel tableModel = (DefaultTableModel) this.table.getModel();
		Object[] val = new Object[tableModel.getRowCount()];
		
		for(int i = 0; i < tableModel.getRowCount(); i++){
			val[i] = table.getValueAt(i, 0);
			if(val[i].toString().equals(username)){
				tableModel.removeRow(i);
			}
		}
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SipServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SipServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SipServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SipServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		
		
		try {
			SipUsers dialog = new SipUsers();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public SipUsers() {
		setTitle("Registered Users");
		table.setAutoCreateRowSorter(true);
		setBounds(100, 100, 450, 300);
		okButton.addActionListener(this);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 437, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 231, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		
		scrollPane.setViewportView(table);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		table.setModel(new javax.swing.table.DefaultTableModel(
	            new Object [][] {

	            },
	            new String [] {
	                "Username", "Address"
	            }
	        ) {
	            /**
				 * 
				 */
				private static final long serialVersionUID = 1L;
				@SuppressWarnings("rawtypes")
				Class[] types = new Class [] {
	                java.lang.String.class, java.lang.String.class
	            };
	            boolean[] canEdit = new boolean [] {
	                false, false
	            };

	            @SuppressWarnings({ "unchecked", "rawtypes" })
				public Class getColumnClass(int columnIndex) {
	                return types [columnIndex];
	            }

	            public boolean isCellEditable(int rowIndex, int columnIndex) {
	                return canEdit [columnIndex];
	            }
	        });
		
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		dispose();
	}
}
