package sip.proxy.main;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.util.ArrayList;

public class SipXML {
	private String ip;
	private int port;
	private String protocol;
		
	public void xmlRead() {
		try{
						
			File file = new File("src/config.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			doc.getDocumentElement().normalize();
		 
			NodeList nList = doc.getElementsByTagName("config");
			Node nNode = nList.item(0);
			Element e = (Element) nNode;
			
			setIp(e.getElementsByTagName("ip").item(0).getTextContent());
			setPort(Integer.parseInt(e.getElementsByTagName("port").item(0).getTextContent()));
			setProtocol(e.getElementsByTagName("protocol").item(0).getTextContent());
			System.out.println("Debug: xml file succesfully parsed !\n");
		}catch(Exception e){
			e.getMessage();
		}
		
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
}	
