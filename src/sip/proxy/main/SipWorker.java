package sip.proxy.main;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import javax.sip.*;

import javax.sip.header.*;
import javax.sip.message.*;


public class SipWorker {

    public static Hashtable<Request,ServerTransaction> serverTransactions= new Hashtable<Request, ServerTransaction>();
    public static Hashtable<ServerTransaction,Request> requestTransaction= new Hashtable<ServerTransaction, Request>();
    public static Hashtable<String,ClientTransaction> clientTransactions= new Hashtable<String, ClientTransaction>();
	static Set<Request> allRequests = new HashSet<Request>();
	static Set<ServerTransaction> allServerTransactions = new HashSet<ServerTransaction>();
	public static Hashtable<String,Request> branchRequests= new Hashtable<String, Request>();
	public static Hashtable<String,ServerTransaction> branchServer= new Hashtable<String, ServerTransaction>();

	public static String[] contactParser(ContactHeader contactHeader){
		 String contact = contactHeader.toString();
		 String[] contact_parser = contact.split("[:;@]");

		 return contact_parser;
	}
	
	public static String[] sipParser(String value, String delims){
		return value.split(delims);
	}

	@SuppressWarnings("rawtypes")
	public static Request gimmeRequest(Header callId){
	//public static Request gimmeRequest(String branch){
		Iterator itr = allRequests.iterator();
        
		while(itr.hasNext()){
            Request a = (Request) itr.next();
      
            if(a.getHeader("Call-Id").equals(callId)){
            	return a;
            }
        }
		
		return null;
	}
	
	public static Request gimmeRequest(String branch){
		ServerTransaction server = branchServer.get(branch);
		Request request = requestTransaction.get(server);

		return request;
	}

	public static ServerTransaction gimmeServerTransaction(Request request){
		@SuppressWarnings("rawtypes")
		Iterator itr = allServerTransactions.iterator();
		
		while(itr.hasNext()){
            ServerTransaction a = (ServerTransaction) itr.next();

            if(a.getRequest().getHeader("Cseq").equals(request.getHeader("Cseq"))){
            	
            	return a;
            }
        }
		
		return null;
	}
}
