package sip.proxy.main;
import gov.nist.javax.sip.header.RecordRoute;
import gov.nist.javax.sip.header.Route;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.net.*;
import java.util.*;

import javax.sdp.SdpFactory;
import javax.sdp.SessionDescription;
import javax.sip.*;
import javax.sip.address.*;
import javax.sip.header.*;
import javax.sip.message.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import sip.proxy.messages.SipCallMessages;
import sip.proxy.messages.SipRequests;
import sip.proxy.messages.SipResponses;
import sip.proxy.messages.GUI.SipDetailMessages;
import sip.proxy.messages.GUI.SipMessages;
import sip.proxy.messages.GUI.SipVirtualize;


@SuppressWarnings({ "serial", "unused" })
public class SipServer extends JFrame implements SipListener{

	
	private JPanel contentPane;
	private SipFactory sipFactory;
	private SdpFactory sdpFactory;
    private SipStack sipStack;
    private SipProvider sipProvider;
    private MessageFactory messageFactory;
    private HeaderFactory headerFactory;
    private AddressFactory addressFactory;
    private ListeningPoint listeningPoint;
    private Properties properties;

    private String ip;
    private int port;
    private String protocol;
    private int tag = (new Random()).nextInt();
    private Address contactAddress;
    private ContactHeader contactHeader;
    private String current_interface = "wlan0";
    boolean DEBUG = false;
    private javax.swing.JTextArea jTextArea;

	private javax.swing.JScrollPane jScrollPaneTable;
    private javax.swing.JScrollPane jScrollPaneText;
    private javax.swing.JTable jTable;
  
    private JMenuBar menuBar;
    private JMenu mnProgram;
    private JMenu mnCleaner;
    private JMenuItem mntmClear;
    private JMenuItem mntmClear2;
    private JMenuItem showMessages;
    private JMenuItem showDetailMessages;
    private JMenuItem showUsers;
    private JMenuItem virtualize;
    
    
    SipMessages sipMessages = new SipMessages();
    SipDetailMessages sipDetailMessages;
    SipUsers sipUsers = new SipUsers();
    SipVirtualize sipVirtualize = new SipVirtualize();
    SipXML sipXML = new SipXML();
    SipRequests sipRequests = new SipRequests();
    SipResponses sipResponses = new SipResponses();
    SipCallMessages sipCallMessages = null;
    ArrayList<SipCallMessages> messagesArrayList = new ArrayList<SipCallMessages>();
    int counter = 0;
    
    public SipServer() {
        initComponents();    
    }
    
    private void initComponents() {
        jScrollPaneTable = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        jScrollPaneText = new javax.swing.JScrollPane();
        jTextArea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SIP Server");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                onOpen(evt);
            }
        });

        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "URI", "From", "To", "Call-ID", "CSeq", "Dialog", "Transaction", "Type", "Request/Response"
            }
        ) {
            /**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			@SuppressWarnings("rawtypes")
			Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            @SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPaneTable.setViewportView(jTable);

        jTextArea.setEditable(false);
        jTextArea.setColumns(20);
        jTextArea.setRows(5);
        jScrollPaneText.setViewportView(jTextArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addComponent(jScrollPaneTable, GroupLayout.DEFAULT_SIZE, 677, Short.MAX_VALUE)
        		.addComponent(jScrollPaneText, GroupLayout.DEFAULT_SIZE, 677, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(Alignment.TRAILING, layout.createSequentialGroup()
        			.addComponent(jScrollPaneTable, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(jScrollPaneText, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE))
        );
        getContentPane().setLayout(layout);

        pack();
        
        menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        
        mnProgram = new JMenu("Program");
        mnCleaner = new JMenu("Cleaner");
        
        menuBar.add(mnProgram);
        menuBar.add(mnCleaner);
        
        
        mntmClear = new JMenuItem(new AbstractAction("Clear TextArea") {
            public void actionPerformed(ActionEvent e) {
                jTextArea.setText("");
            }
        });
        
        mntmClear2 = new JMenuItem(new AbstractAction("Clear Table") {
            public void actionPerformed(ActionEvent e) {
            	DefaultTableModel model = (DefaultTableModel) jTable.getModel();
            	model.setRowCount(0);
            }
        });
        
        showMessages = new JMenuItem(new AbstractAction("Show Messages") {
            public void actionPerformed(ActionEvent e) {
            	//DefaultTableModel model = (DefaultTableModel) jTable.getModel();
            	sipMessages.setVisible(true);
            }
        });
        
        showUsers = new JMenuItem(new AbstractAction("Show Users") {
            public void actionPerformed(ActionEvent e) {
            	sipUsers.setVisible(true);
            }
        });
        		
        virtualize = new JMenuItem(new AbstractAction("Virtualize") {
            
			public void actionPerformed(ActionEvent e) {
            	sipVirtualize.setVisible(true);
	
            }
        });
        
        showDetailMessages = new JMenuItem(new AbstractAction("Show DetailMessages") {
            
			public void actionPerformed(ActionEvent e) {
				sipDetailMessages = new SipDetailMessages();
            	sipDetailMessages.setVisible(true);
            	sipDetailMessages.setArrayList(messagesArrayList);
            }
        });
        
        mnCleaner.add(mntmClear);
        mnCleaner.add(mntmClear2);
        mnProgram.add(showMessages);
        mnProgram.add(showDetailMessages);
        mnProgram.add(showUsers);
        mnProgram.add(virtualize);
        
        jTable.setAutoCreateRowSorter(true);
        
    }
    
    private void onOpen(java.awt.event.WindowEvent evt) {
        try {
        	sipXML.xmlRead();
        	
        	this.ip = sipXML.getIp();
        	this.port = sipXML.getPort();
        	this.protocol = sipXML.getProtocol();
            
        	this.sipFactory = SipFactory.getInstance();
        	this.sdpFactory = SdpFactory.getInstance();
            this.sipFactory.setPathName("gov.nist");
            this.properties = new Properties();
            this.properties.setProperty("javax.sip.STACK_NAME", "stack");
            this.sipStack = this.sipFactory.createSipStack(this.properties);
            this.messageFactory = this.sipFactory.createMessageFactory();
            this.headerFactory = this.sipFactory.createHeaderFactory();
            this.addressFactory = this.sipFactory.createAddressFactory();
            this.listeningPoint = this.sipStack.createListeningPoint(this.ip, this.port, this.protocol);
            this.sipProvider = this.sipStack.createSipProvider(this.listeningPoint);
            this.sipProvider.addSipListener(this);

            this.contactAddress = this.addressFactory.createAddress("sip:" + this.ip + ":" + this.port);
            this.contactHeader = this.headerFactory.createContactHeader(contactAddress);
            
            sipRequests.setFactories(sipFactory.createAddressFactory(), sipFactory.createHeaderFactory());
            sipRequests.setConfig(sipXML.getIp(), sipXML.getPort(), sipXML.getProtocol());
            
            
            this.jTextArea.append("Communicating on address: " + this.ip + ":" + this.port + "\n");

        }
        catch(Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); 
        }
    }
    
	
	public static void main(String[] args) {
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SipServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SipServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SipServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SipServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new SipServer().setVisible(true); 
            }
        });
	}
	
	
	public void processRequest(RequestEvent requestEvent) {
        Request request = requestEvent.getRequest();
        Response response = null;
        
        if(sipCallMessages == null)
        	sipCallMessages = new SipCallMessages();

        jTextArea.append("\nRECV " + request.getMethod() + " " + request.getRequestURI().toString());  
        sipMessages.setTextArea("\nREQUEST MESSAGE RECEIVED:\n" + request.toString()+"\n");
        
       MaxForwardsHeader max = (MaxForwardsHeader) request.getHeader("Max-forwards");
        try {
			max.decrementMaxForwards();
			request.setHeader(max);
		} catch (TooManyHopsException e1) {
			e1.printStackTrace();
		}
        
        try {
        	ServerTransaction transaction = requestEvent.getServerTransaction();
            if(null == transaction) {
                transaction = sipProvider.getNewServerTransaction(request);
            }
        	
            updateTable(requestEvent, request, transaction);
            
            SipWorker.allRequests.add(request);
            SipWorker.serverTransactions.put(request, transaction);
            SipWorker.requestTransaction.put(transaction, request);
            SipWorker.allServerTransactions.add(transaction);
            
			if(request.getMethod().equals("REGISTER")) {
            	sipRequests.handleRegister(request, sipUsers);
            	
            	response = this.messageFactory.createResponse(200, request);
                ((ToHeader)response.getHeader("To")).setTag(String.valueOf(this.tag));
                response.addHeader(this.contactHeader);
                
                response = messageFactory.createResponse(200, request);
                transaction.sendResponse(response);
                
                jTextArea.append(" / SENT " + response.getStatusCode() + " " + response.getReasonPhrase());
                sipMessages.setTextArea("\nRESPONSE MESSAGE RECEIVED & SENT: " + response.getStatusCode() + " " +response.getReasonPhrase());
    			sipMessages.setTextArea( "\n"+response.toString());
                
                sipVirtualize.updateTableRegister(request.getMethod().toString());
                sipVirtualize.updateTableRegisterOk(response.getStatusCode() + " " + response.getReasonPhrase());
            }
            else if(request.getMethod().equals("INVITE")) {         
            	Request requestNew = sipRequests.handleInvite(request, sipUsers,jTextArea);
                
            	if(requestNew == null){
            		response = messageFactory.createResponse(404, request);
                    transaction.sendResponse(response);
                    
                    sipMessages.setTextArea("\nRESPONSE MESSAGE RECEIVED & SENT: " + response.getStatusCode() + " " +response.getReasonPhrase());
        			sipMessages.setTextArea( "\n"+response.toString());
        			sipVirtualize.updateTableRegisterOk(response.getStatusCode() + " " +response.getReasonPhrase());
        			handleCallFlow(request,transaction);
        			handleCallFlow(response,transaction);
        			
        			return;
            	}
            	
	            	try{
	
	                	ClientTransaction clientTransaction = sipProvider.getNewClientTransaction(requestNew);   
	                    
	                    
	                    SipWorker.clientTransactions.put(requestNew.getHeader("Call-Id").toString(), clientTransaction);
	                    clientTransaction.sendRequest();
	                    
	                    SipWorker.branchServer.put(clientTransaction.getBranchId().toString()+clientTransaction.getRequest().getHeader("Cseq").toString(), transaction);

	                    response = messageFactory.createResponse(100, request);
	                    transaction.sendResponse(response);
	                    sipMessages.setTextArea("\nREQUEST MESSAGE SENT:\n" + requestNew.toString()+"\n");
	                    sipVirtualize.updateTableRegisterOk(response.getStatusCode() + " " + response.getReasonPhrase());
	                    
	                    sipVirtualize.updateTableRequest(requestNew.getMethod().toString());
	                }catch(Exception e){
	                	jTextArea.append("\nRequest sent failed: " + e.getMessage() + "\n");
	                }
            	
            }
            
            else if(request.getMethod().equals("ACK")) {
            	
            	ClientTransaction transactionACK = null;
            	
            	Request requestNew = sipRequests.handleAck(request, sipUsers, jTextArea);
            	
            	if(SipWorker.clientTransactions.containsKey(request.getHeader("Call-Id").toString()))
            		transactionACK = SipWorker.clientTransactions.get(request.getHeader("Call-Id").toString());
            	
            	Dialog tdialog = transactionACK.getDialog();
            	tdialog.sendAck(requestNew);
            	sipVirtualize.updateTableRequest(requestNew.getMethod().toString());
            	sipMessages.setTextArea("\nREQUEST MESSAGE SENT:\n" + requestNew.toString()+"\n");
            }
            else if(request.getMethod().equals("CANCEL")) {
            	try{		
            		Request cancelRequest = sipRequests.handleCancel(request, sipProvider);
            		
	                ClientTransaction cancelTransaction = sipProvider.getNewClientTransaction(cancelRequest);
	                
	                SipWorker.branchServer.put(cancelTransaction.getBranchId().toString()+cancelTransaction.getRequest().getHeader("Cseq").toString(), transaction);
	                
	                cancelTransaction.sendRequest();
	                sipVirtualize.updateTableRequest(cancelRequest.getMethod().toString());
            		sipMessages.setTextArea("\nREQUEST MESSAGE SENT:\n" + cancelRequest.toString()+"\n");
            	}catch(Exception e){
                	jTextArea.append("\nRequest sent failed: " + e.getMessage() + "\n");
                	jTextArea.append("in CANCEL\n");
                }
             }
            else if(request.getMethod().equals("BYE")) {
            	ClientTransaction transactionBYE = null;
            	Request requestNew = sipRequests.handleBye(request, sipUsers,jTextArea);
            	
            	if(SipWorker.clientTransactions.containsKey(request))
            		transactionBYE = SipWorker.clientTransactions.get(request.getHeader("Call-Id").toString());
            	
            	sipProvider.sendRequest(requestNew);
            	sipVirtualize.updateTableRequest(requestNew.getMethod().toString());
            	sipMessages.setTextArea("\nREQUEST MESSAGE SENT:\n" + requestNew.toString()+"\n");
            }
			
			handleCallFlow(request,transaction);
			if(response != null){
		        handleCallFlow(response,transaction);  	
		    }
       		        
        }
        catch(SipException e) {            
            this.jTextArea.append("\nERROR (SIP): " + e.getMessage());
        }
        catch(Exception e) {
            this.jTextArea.append("\nERROR: " + e.getMessage());
           e.printStackTrace();
        }
          
    }
  
    @Override
    public void processResponse(ResponseEvent responseEvent) {
    	Response response = responseEvent.getResponse();
    	Request request;
    	
    	/*MaxForwardsHeader max = (MaxForwardsHeader) response.getHeader("Max-forwards");
        try {
			max.decrementMaxForwards();
			response.setHeader(max);
		} catch (TooManyHopsException e1) {
			
			e1.printStackTrace();
		}*/
    	
    	ServerTransaction transaction = null;
    	
    	jTextArea.append("\nRESP " +response.getStatusCode() + " " +response.getReasonPhrase());
    	if(response.getStatusCode() != 100)sipVirtualize.updateTableResponse(response.getStatusCode() + " " +response.getReasonPhrase());
    	
    	try{	
    			Response responseNew = sipResponses.handleResponse((Response) response.clone(), this.contactHeader);
    			
    			transaction = sipResponses.getServerTransaction(response, responseEvent);
    			transaction.sendResponse(responseNew);
    			  
    			sipMessages.setTextArea("\nRESPONSE MESSAGE RECEIVED & SENT: " + response.getStatusCode() + " " +response.getReasonPhrase());
    			sipMessages.setTextArea( "\n"+responseNew.toString());
    			updateTable(responseEvent,response,  transaction, sipResponses.getRequest());
    	}
        catch(Exception e) {
        	
        }
    	
    	handleCallFlow(response, transaction);
    	
    	
    }

 
    private void updateTable(ResponseEvent responseEvent, Response response, ServerTransaction transaction, Request request){
    	DefaultTableModel tableModel = (DefaultTableModel) this.jTable.getModel();
    	
    	FromHeader from = (FromHeader)response.getHeader("From");
    	ToHeader to = (ToHeader)response.getHeader("To");
    	CallIdHeader callId = (CallIdHeader)response.getHeader("Call-Id");
    	CSeqHeader cSeq = (CSeqHeader)response.getHeader("CSeq");
    	Dialog dialog = transaction.getDialog();
    	tableModel.addRow(new Object[] {
    			request.getRequestURI() != null ? request.getRequestURI().toString() : "(unknown)",
                from != null ? from.getAddress() : "(unknown)",
                to != null ? to.getAddress() : "(unknown)",
                callId != null ? callId.getCallId() : "(unknown)",
                cSeq != null ? cSeq.getSeqNumber() + " " + cSeq.getMethod() : "(unknown)",
                dialog != null ? dialog.getDialogId() : "",
                transaction.getBranchId(),
                "Response",
                response.getReasonPhrase() });
    	
    }
    
    private void updateTable(RequestEvent requestEvent, Request request, ServerTransaction transaction) {
        
        DefaultTableModel tableModel = (DefaultTableModel) this.jTable.getModel();
        
        FromHeader from = (FromHeader)request.getHeader("From");
        ToHeader to = (ToHeader)request.getHeader("To");
        CallIdHeader callId = (CallIdHeader)request.getHeader("Call-Id");
        CSeqHeader cSeq = (CSeqHeader)request.getHeader("CSeq");
        
        Dialog dialog = transaction.getDialog();
        
        tableModel.addRow(new Object[] {
            request.getRequestURI() != null ? request.getRequestURI().toString() : "(unknown)",
            from != null ? from.getAddress() : "(unknown)",
            to != null ? to.getAddress() : "(unknown)",
            callId != null ? callId.getCallId() : "(unknown)",
            cSeq != null ? cSeq.getSeqNumber() + " " + cSeq.getMethod() : "(unknown)",
            dialog != null ? dialog.getDialogId() : "",
            transaction.getBranchId(),
            "Request",
            request.getMethod() });
    }
    
    private String getNetworkInterface(){
    	String ip = "";
    	
    	Enumeration<NetworkInterface> interfaces;
		try {
			interfaces = NetworkInterface.getNetworkInterfaces();
			
			while(interfaces.hasMoreElements()){
	    		NetworkInterface ni = (NetworkInterface) interfaces.nextElement();
	    		Enumeration<InetAddress> addresses = ni.getInetAddresses();
	    		while (addresses.hasMoreElements()){
	    			InetAddress inetAddress = (InetAddress) addresses.nextElement();
	    			if(ni.getDisplayName().equals(current_interface)){
	    				if(inetAddress.getHostAddress().matches("\\d+\\.\\d+\\.\\d+\\.\\d+"))
	    					ip = inetAddress.getHostAddress();
	    			}
	    		}
	    	}
			
		} catch (SocketException e) {
			System.out.println("Error while getting network interface!");
		}
    	
		return ip;
    }

	@Override
	public void processDialogTerminated(DialogTerminatedEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processIOException(IOExceptionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processTimeout(TimeoutEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processTransactionTerminated(TransactionTerminatedEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	public void handleCallFlow(Request request, ServerTransaction transaction){
		for(int i = 0; i < messagesArrayList.size(); i++){
        	if(messagesArrayList.get(i).getCallId().equals(request.getHeader("Call-ID").toString())){
        		sipCallMessages = messagesArrayList.get(i);
        		//System.out.println("Nasiel som request call id v objekte "+sipCallMessages);
        	}
        }
		
		 if(sipCallMessages.getCallId().equals(request.getHeader("Call-ID").toString())){
			 if(sipCallMessages.getCseqBranch(transaction.getBranchId().toString()) == null){
				 sipCallMessages.setCseqBranch(transaction.getBranchId().toString());
				 //if(sipCallMessages.getMessages(request.toString()) == null){
	        			sipCallMessages.setMessages(request.toString());
	        			
	        		//}
	        	}else{
	        		//if(sipCallMessages.getMessages(request.toString()) == null){
	        			sipCallMessages.setMessages(request.toString());	
	        		//}
	        	}
			 
			 
	        }else{
	        	sipCallMessages = new SipCallMessages();
	        	sipCallMessages.setCallId(request.getHeader("Call-id").toString());
	        	sipCallMessages.setCseqBranch(transaction.getBranchId().toString());
	        	sipCallMessages.setMessages(request.toString());
	        	
	        	
	        }
	        
	        for(int i = 0; i < messagesArrayList.size(); i++){
	        	if(!messagesArrayList.get(i).getCallId().equals(sipCallMessages.getCallId())){
	        		counter++;
	        		//System.out.println("Nenachadza sa, zvysujem counter "+counter);
	        	}
	        }
	        if(counter == messagesArrayList.size()){
	        	//System.out.println("ukladam call-id "+sipCallMessages.getCallId());
	        	messagesArrayList.add(sipCallMessages);
	        }
	        counter = 0;
	}
	
	public void handleCallFlow(Response response, ServerTransaction transaction){
		for(int i = 0; i < messagesArrayList.size(); i++){
        	if(messagesArrayList.get(i).getCallId().equals(response.getHeader("Call-ID").toString())){
        		sipCallMessages = messagesArrayList.get(i);
        		//System.out.println("Nasiel som request call id v objekte "+sipCallMessages);
        	}
        }
		
		 if(sipCallMessages.getCallId().equals(response.getHeader("Call-ID").toString())){
			 if(sipCallMessages.getCseqBranch(transaction.getBranchId().toString()) == null){
				 sipCallMessages.setCseqBranch(transaction.getBranchId().toString());
				 //if(sipCallMessages.getMessages(request.toString()) == null){
	        			sipCallMessages.setMessages(response.toString());
	        			
	        		//}
	        	}else{
	        		//if(sipCallMessages.getMessages(request.toString()) == null){
	        			sipCallMessages.setMessages(response.toString());	
	        		//}
	        	}
			 
			 
	        }else{
	        	sipCallMessages = new SipCallMessages();
	        	sipCallMessages.setCallId(response.getHeader("Call-id").toString());
	        	sipCallMessages.setCseqBranch(transaction.getBranchId().toString());
	        	sipCallMessages.setMessages(response.toString());
	        	
	        	
	        }
	        
	        for(int i = 0; i < messagesArrayList.size(); i++){
	        	if(!messagesArrayList.get(i).getCallId().equals(sipCallMessages.getCallId())){
	        		counter++;
	        		//System.out.println("Nenachadza sa, zvysujem counter "+counter);
	        	}
	        }
	        if(counter == messagesArrayList.size()){
	        	//System.out.println("ukladam call-id "+sipCallMessages.getCallId());
	        	messagesArrayList.add(sipCallMessages);
	        }
	        counter = 0;
	}
	
}
