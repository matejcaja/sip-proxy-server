package sip.proxy.messages;

import javax.sip.ResponseEvent;
import javax.sip.ServerTransaction;
import javax.sip.header.Header;
import javax.sip.message.Request;
import javax.sip.message.Response;

import sip.proxy.main.SipWorker;

public class SipResponses {
	private Request request;
	
	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public ServerTransaction getServerTransaction(Response response, ResponseEvent responseEvent){
		ServerTransaction transaction = null;
		
		//Request request = SipWorker.gimmeRequest(response.getHeader("Call-Id"));
		

		Request request = SipWorker.gimmeRequest(responseEvent.getClientTransaction().getBranchId().toString()+responseEvent.getClientTransaction().getRequest().getHeader("Cseq").toString());
		setRequest(request);
		
		transaction = SipWorker.gimmeServerTransaction(request);
		/*if(SipWorker.serverTransactions.containsKey(request))
			transaction = SipWorker.serverTransactions.get(request);*/
		//return SipWorker.serverTransactions.get(request);
		return transaction;
		//return SipWorker.branchServer.get(responseEvent.getClientTransaction().getBranchId().toString()+responseEvent.getClientTransaction().getRequest().getHeader("Cseq").toString());
	}
	
	public Response handleResponse(Response responseNew, Header contactHeader){
		if(!responseNew.getHeader("Cseq").toString().contains("CANCEL"))
			responseNew.removeFirst("Via");
		
		responseNew.addHeader(contactHeader);
		
		return responseNew;
	}
	
	
}
