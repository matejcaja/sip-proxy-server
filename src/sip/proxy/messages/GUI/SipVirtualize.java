package sip.proxy.messages.GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import sip.proxy.main.SipServer;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class SipVirtualize extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTable table;
	JButton okButton = new JButton("Close");
	JButton cancelButton = new JButton("Show Call");
	
	
	public void updateTableResponse(String response) {
		DefaultTableModel tableModel = (DefaultTableModel) this.table.getModel();
		tableModel.addRow(new Object[] {response,"<---",response, "<---", response});

	}
	

	public void updateTableRegisterOk(String response) {
		DefaultTableModel tableModel = (DefaultTableModel) this.table.getModel();
		tableModel.addRow(new Object[] {response,"<---",response, "",""});
		
	}
	
	public void updateTableRequest(String request){
		DefaultTableModel tableModel = (DefaultTableModel) this.table.getModel();
		tableModel.addRow(new Object[] {request,"--->",request, "--->", request});
	}
	
	public void updateTableRegister(String request) {
		DefaultTableModel tableModel = (DefaultTableModel) this.table.getModel();
		tableModel.addRow(new Object[] {request,"--->",request, "",""});
		
	}
	
	public static void main(String[] args) {
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SipServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SipServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SipServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SipServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		
		try {
			SipVirtualize dialog = new SipVirtualize();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public SipVirtualize() {
		setBounds(100, 100, 550, 400);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 534, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
		);
		{
			table = new JTable();
			scrollPane.setViewportView(table);
			table.setModel(new javax.swing.table.DefaultTableModel(
		            new Object [][] {

		            },
		            new String [] {
		                "User1","","Proxy","" ,"User2"
		            }
		        ) {
		            /**
					 * 
					 */
					private static final long serialVersionUID = 1L;
					@SuppressWarnings("rawtypes")
					Class[] types = new Class [] {
		                java.lang.String.class, java.lang.String.class,java.lang.String.class,java.lang.String.class,java.lang.String.class
		            };
		            boolean[] canEdit = new boolean [] {
		                false, false, false, false,false
		            };

		            @SuppressWarnings({ "unchecked", "rawtypes" })
					public Class getColumnClass(int columnIndex) {
		                return types [columnIndex];
		            }

		            public boolean isCellEditable(int rowIndex, int columnIndex) {
		                return canEdit [columnIndex];
		            }
		        });
			
			
		}
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}			
			{
				
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			
		}
		
		okButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	dispose();

		    }
		});
		
		cancelButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        
		    }
		});
			
	}


	

	

}
