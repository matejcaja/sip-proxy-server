package sip.proxy.messages.GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.LayoutStyle.ComponentPlacement;

import sip.proxy.messages.SipCallMessages;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JLabel;

public class SipDetailMessages extends JDialog {
	JComboBox comboBox = new JComboBox();
	JComboBox comboBox_1 = new JComboBox();
	JButton okButton = new JButton("Show");
	JButton cancelButton = new JButton("Close");
	
	private final JPanel contentPanel = new JPanel();
	ArrayList<SipCallMessages> messagesArrayList = new ArrayList<SipCallMessages>();
	private final JScrollPane scrollPane = new JScrollPane();
	JTextArea textArea = new JTextArea();
	private final JLabel lblCallid = new JLabel("Call-ID");
	private final JLabel lblCseqbranch = new JLabel("Cseq/Branch");
	
	
	
	public void setArrayList(ArrayList<SipCallMessages> messagesArrayList) {
		this.messagesArrayList = messagesArrayList;
		
		for(int i = 0; i < messagesArrayList.size(); i++){
			comboBox.addItem( i+"");
		}
		
	}
	
	public void okButtonProcess(){
		SipCallMessages sipCallMessages = messagesArrayList.get(comboBox.getSelectedIndex());
		
		for(int i = 0; i < sipCallMessages.getMessagesSize(); i++){
    		if(! comboBox_1.getSelectedItem().equals("-")){
    			String cseq = sipCallMessages.getCseqBranchFromIndex(Integer.parseInt(comboBox_1.getSelectedItem().toString()));
    			
    			if(sipCallMessages.getMessagesFromIndex(i).contains(cseq)){
    				textArea.append(sipCallMessages.getMessagesFromIndex(i)+"\n");
    			}
    		}else{
    			textArea.append(sipCallMessages.getMessagesFromIndex(i));
    		}
			
    	}
	}
	
	public void comboBox_1Process() {
		
    	comboBox_1.removeAllItems();
    	comboBox_1.addItem("-");
    	for(int i = 0; i < messagesArrayList.get(comboBox.getSelectedIndex()).getCseqSize(); i++){
    		comboBox_1.addItem(i+"");
    	}
		
	}
	
	
	public static void main(String[] args) {
		try {
			SipDetailMessages dialog = new SipDetailMessages();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public SipDetailMessages() {
		setBounds(100, 100, 694, 553);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		scrollPane.setViewportView(textArea);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			
		}
		
		okButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			    textArea.setText("");	
		    	okButtonProcess();}}
		);
		cancelButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			    dispose();
			}
		});
		
		comboBox.setSelectedItem(null);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblCseqbranch)
						.addComponent(lblCallid))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(comboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(comboBox_1, 0, 79, Short.MAX_VALUE))
					.addContainerGap(389, Short.MAX_VALUE))
				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 580, Short.MAX_VALUE)
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(17)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblCallid)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCseqbranch))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		
		comboBox.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	comboBox_1Process();}}
		);
		
	}

	
	
	public void actionPerformed(ActionEvent arg0) {}
	
	
}
