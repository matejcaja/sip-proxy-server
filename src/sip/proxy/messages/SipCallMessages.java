package sip.proxy.messages;

import java.util.ArrayList;

public class SipCallMessages {
	private String callId = new String();
	private ArrayList<String> cseqBranch = new ArrayList<String>();
	private ArrayList<String> messages = new ArrayList<String>();
	
	
	public String getCallId() {
		return callId;
	}


	public void setCallId(String callId) {
		this.callId = callId;
	}
	
	
	public int getCseqSize(){
		return cseqBranch.size();
	}
	
	public int getMessagesSize(){
		return messages.size();
	}
	
	public String getMessagesFromIndex(int i){
		return messages.get(i);
	}

	public String getCseqBranchFromIndex(int index){
		return cseqBranch.get(index);
	}
	
	public String getCseqBranch(String branch) {
		for(int i = 0; i < cseqBranch.size(); i++){
			if(cseqBranch.get(i).equals(branch)){
				return cseqBranch.get(i);
			}
		}
		return null;
	}
	
	public void printCseqBranch(){
		for(int i = 0; i < cseqBranch.size(); i++){
			System.out.println(cseqBranch.get(i));
		}
	}

	
	public void printMessages(){
		System.out.println("V messageoch sa nachadza "+messages.size());
		
		for(int i = 0; i < messages.size(); i++){
			System.out.println(messages.get(i));
		}
	}
	
	public void setCseqBranch(String cseqBranch) {
		this.cseqBranch.add(cseqBranch);
	}


	public String getMessages(String message) {
		
		for(int i = 0; i < messages.size(); i++){
			if(cseqBranch.get(i).equals(message)){
				return messages.get(i);
			}
		}
		return null;
	}


	public void setMessages(String message) {
		this.messages.add(message);
	}

}
