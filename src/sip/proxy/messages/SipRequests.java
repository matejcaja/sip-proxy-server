package sip.proxy.messages;

import sip.proxy.main.*;
import javax.sip.*;

import javax.sip.address.Address;
import javax.sip.address.AddressFactory;
import javax.sip.header.*;
import javax.sip.message.*;
import javax.swing.JTextArea;

public class SipRequests {
	private AddressFactory addressFactory;
	private HeaderFactory headerFactory;
	private String ip;
	private int port;
	@SuppressWarnings("unused")
	private String protocol;
	
	
	public void setFactories(AddressFactory addressFactory, HeaderFactory headerFactory){
		this.addressFactory = addressFactory;
		this.headerFactory = headerFactory;
	}
	
	public void setConfig(String ip, int port, String protocol){
		this.ip = ip;
		this.port = port;
		this.protocol = protocol;
	}
	
	public void handleRegister(Request request, SipUsers sipUsers){
		String[] contact = SipWorker.contactParser((ContactHeader)request.getHeader("Contact"));            	            	
    	String[] expires = SipWorker.sipParser(request.getExpires().toString(), "[:]");
    	int expireValue = Integer.parseInt(expires[1].trim());
 
    	sipUsers.handleUserRegister(contact, expireValue);
		
    	
	}
	
	public Request handleInvite(Request request, SipUsers sipUsers, JTextArea jTextArea){
				
		String[] user_to_call = SipWorker.sipParser(request.getRequestURI().toString(), "[:@]");
    	String user = sipUsers.getUser(user_to_call[1]);

    	if(user == null){
    		return null;
    	}
    	
    	try{
    		Address contactAddress = addressFactory.createAddress("sip:"+ user);
    		Address proxyAddress = addressFactory.createAddress("sip:"+ ip+":"+port);
    		javax.sip.address.URI requestURI = contactAddress.getURI();
    	
	    	Request requestNew = (Request)request.clone();
	        ViaHeader viaHeader = headerFactory.createViaHeader(ip, port, "udp", null);
	        
	        RecordRouteHeader recordRouteHeader = headerFactory.createRecordRouteHeader(proxyAddress);
	        
            requestNew.setRequestURI(requestURI);
            requestNew.addFirst(viaHeader);
            requestNew.addHeader(recordRouteHeader);
	        
	        return requestNew;
    	}catch(Exception e){
    		jTextArea.append("\nRequest sent failed: " + e.getMessage() + "\n");
    	}
		
    	return null;
	}
	
	public Request handleCancel(Request request,SipProvider sipProvider) throws SipException{
		Request cancelRequest = null;
		
		if(SipWorker.clientTransactions.containsKey(request.getHeader("Call-Id").toString()))
			cancelRequest = SipWorker.clientTransactions.get(request.getHeader("Call-Id").toString()).createCancel();
		
		
		return cancelRequest;
	}
	
	public Request handleBye(Request request, SipUsers sipUsers, JTextArea jTextArea){
		
		String[] user_to_call = SipWorker.sipParser(request.getHeader("Route").toString(), "[:@]");
		String user = sipUsers.getUser(user_to_call[2]);
		
		try{
    		Address contactAddress = addressFactory.createAddress("sip:"+ user);

    		javax.sip.address.URI requestURI = contactAddress.getURI();
    	
	    	Request requestNew = (Request)request.clone();
	        ViaHeader viaHeader = headerFactory.createViaHeader(ip, port, "udp", null);

            requestNew.setRequestURI(requestURI);
            requestNew.addFirst(viaHeader);
            
	        
	        return requestNew;
    	}catch(Exception e){
    		jTextArea.append("\nRequest sent failed: " + e.getMessage() + "\n");
    	}
		
		
		return null;
		
	}
	
	public Request handleAck(Request request, SipUsers sipUsers, JTextArea jTextArea){

		String[] user_to_call = SipWorker.sipParser(request.getHeader("Route").toString(), "[:@]");
		String user = sipUsers.getUser(user_to_call[2]);

		try{
    		Address contactAddress = addressFactory.createAddress("sip:"+ user);

    		javax.sip.address.URI requestURI = contactAddress.getURI();
    	
	    	Request requestNew = (Request)request.clone();
	        ViaHeader viaHeader = headerFactory.createViaHeader(ip, port, "udp", null);

            requestNew.setRequestURI(requestURI);
            requestNew.addFirst(viaHeader);
            
	        
	        return requestNew;
    	}catch(Exception e){
    		jTextArea.append("\nRequest sent failed: " + e.getMessage() + "\n");
    	}
		
		
		return null;
		
	}
	
	
}
